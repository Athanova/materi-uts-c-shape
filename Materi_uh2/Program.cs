﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Materi_uh2_library_;

namespace Materi_uh2
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 list = new Class1();
            Pegawai pe = new Pegawai();
            Anggota ang = new Anggota();
            Buku book = new Buku(250, 500);

            Console.WriteLine("Apakah anda ingin melihat peraturan perpustakaan");
            Console.WriteLine("1. Yes");
            Console.WriteLine("2. No");
            Console.Write("Masukan pilihan : ");
            int pilihan = int.Parse(Console.ReadLine());
            if (pilihan == 1)
            {
                Console.WriteLine("Peraturan Pegawai :");
                pe.kekerasan();
                pe.merokok();

                Console.WriteLine("Peraturan Anggota :");
                ang.kekerasan();
                ang.merokok();
                ang.peminjaman();

                list.list_buku();
                Console.WriteLine();
                Console.WriteLine("Apa yang ingin anda lakukan : ");
                Console.WriteLine("1. Meminjam");
                Console.WriteLine("2. Mengembalikan");
                Console.Write("Masukan pilihan : ");
                int no = int.Parse(Console.ReadLine());
                if (no == 1)
                {
                    book.pinjam();
                }
                if (no == 2)
                {
                    book.denda();
                }
                else
                {
                    Console.WriteLine("Tidak ada pilihan");
                }
            }
            if (pilihan == 2)
            {
                list.list_buku();
                Console.WriteLine();
                Console.WriteLine("Apa yang ingin anda lakukan : ");
                Console.WriteLine("1. Meminjam");
                Console.WriteLine("2. Mengembalikan");
                Console.Write("Masukan pilihan : ");
                int no = int.Parse(Console.ReadLine());
                if (no == 1)
                {
                    book.pinjam();
                }
                if (no == 2)
                {
                    book.denda();
                }
                else
                {
                    Console.WriteLine("Tidak ada pilihan");
                }
            }
            else
            {
                Console.WriteLine("Tidak ada pilihan");
            }
            Console.ReadLine();
        }
    }
}
