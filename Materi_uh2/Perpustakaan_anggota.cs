﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Materi_uh2
{
    abstract class Perpustakaan_anggota
    {
        public abstract void kekerasan();
        public abstract double peminjaman();
        public abstract void merokok();
    }
}
