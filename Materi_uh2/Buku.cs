﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Materi_uh2
{
    class Buku
    {
        public double angka1, angka2;
        public Buku(double an1, double an2)
        {
            this.angka1 = an1;
            this.angka2 = an2;
        }
        
        public double pinjam()
        {
            Console.Write("Berapa lama anda akan meminjam : ");
            Console.ReadLine();
            Console.WriteLine("Biaya pinjam buku : " + angka1 + " Rupiah");
            return angka1;
        }

        public double denda()
        {
            Console.Write("Pengembalian telat berapa hari : ");
            int d = int.Parse(Console.ReadLine());
            double den = d * angka2;
            Console.Write("Denda anda : " + den +" Rupiah");
            return angka2;
        }
    }
}
