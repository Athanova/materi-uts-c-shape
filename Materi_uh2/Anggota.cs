﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Materi_uh2
{
    class Anggota : Perpustakaan_anggota
    {
        public override void kekerasan()
        {
            Console.WriteLine("Anggota");
            Console.WriteLine("Tidak boleh melakukan kekerasan");
        }

        public override void merokok()
        {
            Console.WriteLine("Tidak boleh merokok didalam perpustakaan");
            
        }

        public override double peminjaman()
        {
            double s = 2;
            Console.WriteLine("Meminjam buku maksimal : " + s + " buah");
            Console.ReadLine();
            Console.WriteLine();
            return s;
        }
    }
}
